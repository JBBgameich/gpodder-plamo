import QtQuick 2.7
import QtQuick.Controls 2.0 as Controls
import io.thp.pyotherside 1.3
import "touch"

Controls.ApplicationWindow {
	visible: true
	title: "gPodder"
	minimumHeight: 700
	minimumWidth: 500
	
	Python {
		Component.onCompleted: {
			addImportPath(dataPath)
		}
	}

	Main {}
}
