# gPodder for Plasma Mobile

This project provides a freedesktop desktop entry,
CMake configuration and proper installation paths for
the gPodder QML interface.

It might develop into a Kirigami based UI for Plasma Mobile,
but currently just uses the default QML touch interface.

## Dependencies
* Dependencies of [gpodder-ui-qml](https://raw.githubusercontent.com/gpodder/gpodder-ui-qml/master/README)
* extra-cmake-modules
* QtQuickControls 2

# Building
```
mkdir build && cd build
cmake -GNinja -DCMAKE_INSTALL_PREFIX=/usr/ ..
ninja install
```

# Copyright
License: GNU GPLv3 or later

gpodder.desktop:
 * Copyright (c) 2005-2018, The gPodder Team

gpodder.svg:
 * Copyright (c) 2005-2018, The gPodder Team

gpodder-ui-qml/*:
 * Copyright (c) 2013-2016, Thomas Perl <m@thp.io>



